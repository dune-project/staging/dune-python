
# coding: utf-8

# # Finite Volume example
#
# (example taken from dune-grid-howto)
#
# Consider the transport equation
# \begin{equation*}
# \partial_t u + \partial_x u + \partial_y u = 0.
# \end{equation*}
#
# Use functon $\bar{u}(t,x,y)$ for initial and boundary conditions.
#
# Explicit scheme:
# \begin{align*}
#    u^0_E &= \bar{u}(0,\omega_T)  \\
#    u^{n+1}_E &= u^n_E - \sum_{\dim(E' \cap E) = 1}
#      g(u^n_E,u^n_{E'},n_E)
# \end{align*}
# using an upwind flux $g$.
#
# Some initialize imports:

# In[ ]:


import time
import numpy
import math


# First we need to set up a grid and a mapper to attach data to the elements of the grid (the codimension zero entities).

# In[ ]:


from dune.grid import cartesianDomain, yaspGrid

domain = cartesianDomain([0, 0], [1, 1], [20, 20])
yaspView = yaspGrid(domain)
yaspView.hierarchicalGrid.globalRefine(1)

from dune.grid.map import MultipleCodimMultipleGeomTypeMapper as Mapper
mapper = Mapper(yaspView, lambda gt: gt.dim == yaspView.dimension)


# Function to set up the initialize data:

# In[ ]:


def initialize(c0):
    c = numpy.zeros(len(mapper))
    for e in yaspView.elements:
        c[mapper.index(e)] = c0(e.geometry.center)
    return c


# Here is the actual evolution operator for the finite volume scheme:

# In[ ]:


def pyevolve(gridView, mapper, c, b, t):
    upd, dt = numpy.zeros(len(c)), 1e100
    for e in gridView.elements:
        volume, idxi = e.geometry.volume, mapper.index(e)
        sumfactor = 0.0
        for i in gridView.intersections(e):
            f = ([1, 1] * i.centerUnitOuterNormal) * i.geometry.volume / volume
            sumfactor += max(f, 0)
            outside = i.outside
            if outside is not None:
                idxj  = mapper.index(outside)
                upd[idxi] -= c[idxj if f < 0 else idxi] * f
            elif i.boundary:
                x = i.geometryInInside.center
                upd[idxi] -= (b(e.geometry.toGlobal(x), t) if f < 0 else c[idxi]) * f
        dt = min(dt, 1.0 / sumfactor)
    dt *= 0.99
    c += upd*dt
    return dt


# Initiale and boundary values

# In[ ]:


c0 = lambda x: 1.0 if x.two_norm > 0.125 and x.two_norm < 0.5 else 0.0
b = lambda x, t: c0(x - [t, t])


# and the time evolution

# In[ ]:


c = initialize(c0)
start = time.time()
t = 0.0
while t < 0.5:
    t += pyevolve(yaspView, mapper, c, b, t)
print("time used:", time.time()-start)


# Here is the final result

# In[ ]:


from dune.plotting import plotPointData
cgrid = yaspView.function(lambda e,p: [c[mapper.index(e)]])
plotPointData(cgrid, figsize=(9,9))


# Let's do this on the C++ side instead:

# In[ ]:


from dune.generator import algorithm

c = initialize(c0)

evolve = algorithm.load('evolve', 'evolve.hh', yaspView, mapper, c, b, 0.0)

start = time.time()
t = 0.0
while t < 0.5:
    t += evolve(yaspView, mapper, c, b, t)
print("time used:", time.time()-start)


# In[ ]:


from dune.plotting import plotPointData
plotPointData(cgrid, figsize=(9,9))


# Now that its a bit faster let us recompute the solution on a finer grid:

# In[ ]:


yaspView.hierarchicalGrid.globalRefine(2)
mapper = Mapper(yaspView, lambda gt: gt.dim == yaspView.dimension)
c = initialize(c0)
start = time.time()
t = 0.0
while t < 0.5:
    t += evolve(yaspView, mapper, c, b, t)
print("time used:", time.time()-start)
plotPointData(cgrid, figsize=(9,9),gridLines="")
