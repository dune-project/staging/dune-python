{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Finite Elements\n",
    "\n",
    "As another example we solve the poisson equation\n",
    "\n",
    "\\begin{align*}\n",
    "    -\\Delta u &= f && \\text{in $\\Omega$}, \\\\\n",
    "    u &= 0 && \\text{auf $\\partial\\Omega$}\n",
    "\\end{align*}\n",
    "\n",
    "in Python based on a simplicial Dune grid: `ALUConformGrid`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import time\n",
    "import numpy\n",
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "As a first step, we construct the grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.grid import cartesianDomain, gridFunction\n",
    "from dune.alugrid import aluConformGrid\n",
    "vertices = numpy.array([(0,0), (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1)])\n",
    "triangles = numpy.array([(2,0,1), (0,2,3), (4,0,3), (0,4,5), (6,0,5), (0,6,7)])\n",
    "aluView = aluConformGrid({\"vertices\": vertices, \"simplices\": triangles})\n",
    "aluView.hierarchicalGrid.globalRefine(7)                         "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class LinearShapeFunction:\n",
    "    def __init__(self, ofs, grad):\n",
    "        self.ofs = ofs\n",
    "        self.grad = grad\n",
    "    def evaluate(self, local):\n",
    "        return [self.ofs + sum([x*y for x, y in zip(self.grad, local)])]\n",
    "    def gradient(self, local):\n",
    "        return self.grad\n",
    "\n",
    "dim = aluView.dimension\n",
    "p1ShapeFunctionSet = [LinearShapeFunction(1.0, [-1.0] * dim)]\n",
    "for i in range(dim):\n",
    "    p1ShapeFunctionSet.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's get going. First we assemble the right hand side with\n",
    "$$\n",
    "    f(x) = 2\\prod_{i} x_i\\,(1-x_i)\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.geometry import quadratureRules\n",
    "from dune.istl import blockVector\n",
    "\n",
    "f = lambda v: [sum(2.0 * x * (1 - x) for x in v)]\n",
    "\n",
    "dim, indexSet = aluView.dimension, aluView.indexSet\n",
    "rhs = blockVector(indexSet.size(dim))\n",
    "\n",
    "quadrature = quadratureRules(2)\n",
    "for e in aluView.elements:\n",
    "    geo = e.geometry\n",
    "    for p in quadrature(e):\n",
    "        x = p.position\n",
    "        w = p.weight * geo.integrationElement(x)\n",
    "        for i, phi in enumerate(p1ShapeFunctionSet):\n",
    "            index = indexSet.subIndex(e, i, dim)\n",
    "            rhs[index] += w * phi.evaluate(x)[0] * f(geo.toGlobal(x))[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we assemble the stiffness matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.istl import BuildMode, bcrsMatrix, BCRSMatrix\n",
    "\n",
    "dim, indexSet = aluView.dimension, aluView.indexSet\n",
    "matrix = bcrsMatrix((indexSet.size(dim), indexSet.size(dim)), 8, 0.1, BuildMode.implicit)\n",
    "\n",
    "quadrature = quadratureRules(1)\n",
    "for e in aluView.elements:\n",
    "    geo = e.geometry\n",
    "    for p in quadrature(e):\n",
    "        x = p.position\n",
    "        w = p.weight * geo.integrationElement(x)\n",
    "        \n",
    "        jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)\n",
    "        grads = [numpy.dot(jit, phi.gradient(x)) for phi in p1ShapeFunctionSet]\n",
    "        \n",
    "        for i, dphi in enumerate(grads):\n",
    "            row = indexSet.subIndex(e, i, dim)\n",
    "            for j, dpsi in enumerate(grads):\n",
    "                matrix[row, indexSet.subIndex(e, j, dim)] += [[numpy.dot(dphi, dpsi) * w]]\n",
    "                \n",
    "_ = matrix.compress()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And apply Dirichlet boundary conditions to the linear system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.geometry import referenceElement\n",
    "\n",
    "for e in aluView.elements:\n",
    "    for i in aluView.intersections(e):\n",
    "        if not i.boundary:\n",
    "            continue\n",
    "        ref = referenceElement(e)\n",
    "        j = i.indexInInside\n",
    "        subs = [ref.subEntity(j, 1, k, dim) for k in range(ref.size(j, 1, dim))]\n",
    "        rows = [indexSet.subIndex(e, s, dim) for s in subs]\n",
    "        for r in rows:\n",
    "            matrix[r] *= 0.0\n",
    "            matrix[r, r] = [[1]]\n",
    "            rhs[r] = 0.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.istl import CGSolver, SeqJacobi\n",
    "\n",
    "solver = CGSolver(matrix.asLinearOperator(), SeqJacobi(matrix), 1e-10)\n",
    "\n",
    "u = blockVector(aluView.indexSet.size(aluView.dimension))\n",
    "_ = solver(u, rhs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "To visualize the result, we need to construct a corresponding grid function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "@gridFunction(aluView)\n",
    "def lgf( element, local ):\n",
    "    return [ sum( phi.evaluate(local) * u[indexSet.subIndex(element, i, dim)] \\\n",
    "                 for i, phi in enumerate(p1ShapeFunctionSet)) ]\n",
    "_ = aluView.writeVTK(\"fem2d\", pointdata={\"u\": lgf})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from dune.plotting import plotPointData\n",
    "plotPointData(lgf, figsize=(9,9), gridLines=None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "livereveal": {
   "center": false,
   "controls": false,
   "height": 810,
   "progress": false,
   "slideNumber": false,
   "start_slideshow_at": "selected",
   "theme": "serif",
   "transition": "none",
   "width": 1440
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
