
# coding: utf-8

# # Integrating dune-functions

# In[1]:


import time
import numpy
import math


# In[ ]:


from dune.grid import cartesianDomain, yaspGrid

domain = cartesianDomain([0, 0], [1, 1], [20, 20])
yaspView = yaspGrid(domain)
yaspView.hierarchicalGrid.globalRefine(1)

from dune.grid.map import MultipleCodimMultipleGeomTypeMapper as Mapper
mapper = Mapper(yaspView, lambda gt: gt.dim == yaspView.dimension)


# In[ ]:


try:
    from dune.functions import defaultGlobalBasis, Lagrange, Power, Composite
except ImportError:
    print("""
    Requires dune-py to be configured with a dune-functions module available.
         """)
    exit(1)

lagrangeBasis = defaultGlobalBasis(yaspView, Lagrange(2))
print(str(Lagrange(2)) + ".dimension:", lagrangeBasis.dimension)

lagrangeBasis = defaultGlobalBasis(yaspView, Lagrange(1,2))
print(str(Lagrange(1,2)) + ".dimension:", lagrangeBasis.dimension)

lagrangeBasis = defaultGlobalBasis(yaspView, Power(Lagrange(1),2))
print(str(Power(Lagrange(1),2)) + ".dimension:", lagrangeBasis.dimension)


# In[ ]:


from dune.grid import cartesianDomain
from dune.alugrid import aluCubeGrid

domain = cartesianDomain([0, 0], [1, 1], [50, 50])
aluView = aluCubeGrid(domain)

taylorHood = Lagrange(2)**2 * Lagrange(1)
taylorHoodBasis = defaultGlobalBasis(aluView, taylorHood)
print("nodes*3 + edges*2 + elements*2:", aluView.size(2)*3 + aluView.size(1)*2 + aluView.size(0)*2)
print(str(taylorHood) + ".dimension:", taylorHoodBasis.dimension)


# In[ ]:


from dune.generator import algorithm

data, colIndex, rowIndex = [], [], []
lv, lis = taylorHoodBasis.localView(), taylorHoodBasis.localIndexSet()
assemble = algorithm.load('assemble', 'taylorhood.hh', lv, numpy.zeros([1,1]))
for element in aluView.elements():
    lv.bind(element), lis.bind(lv)

    localM = numpy.zeros([len(lv), len(lv)])
    assemble(lv, localM)

    for i in range(len(lv)):
        row = lis.index(i)
        for j in range(len(lv)):
            col = lis.index(j)
            data.append(localM[i][j])
            colIndex.append(col[0])
            rowIndex.append(row[0])

    lis.unbind(), lv.unbind()


# In[ ]:


from scipy.sparse import coo_matrix
matrix = coo_matrix((data, (colIndex, rowIndex))).tocsr()
rhs_dofs = numpy.zeros(taylorHoodBasis.dimension)


# In[ ]:

from dune.generator import algorithm
def onBoundary(x):
    return any(x[i] < 1e-10 or x[i] > (1-1e-10) for i in range(len(x)))
rows, values = algorithm.run('boundary', 'taylorhood.hh',
                             taylorHoodBasis, onBoundary)


# In[ ]:


# delete rows
for r, v in zip(rows, values):
    matrix.data[matrix.indptr[r]:matrix.indptr[r+1]] = 0.0
    rhs_dofs[r] = v
# Set the diagonal
d = matrix.diagonal()
d[rows] = 1.0
matrix.setdiag(d)


# In[ ]:


from scipy.sparse.linalg import spsolve
solution_dofs = spsolve(matrix, rhs_dofs)
stokes_solution = taylorHoodBasis.asFunction(solution_dofs)


# In[ ]:


from dune.plotting import plotComponents, plotPointData
plotComponents(stokes_solution, level=2, gridLines=None,figsize=(15,5))
plotPointData(stokes_solution, vectors=[0,1], nofVectors=4000, gridLines=None, figsize=(5,5))
