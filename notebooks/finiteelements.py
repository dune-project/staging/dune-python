
# coding: utf-8

# # Finite Elements
#
# As another example we solve the poisson equation
#
# \begin{align*}
#     -\Delta u &= f && \text{in $\Omega$}, \\
#     u &= 0 && \text{auf $\partial\Omega$}
# \end{align*}
#
# in Python based on a simplicial Dune grid: `ALUConformGrid`.

# In[ ]:


import time
import numpy
import math


# As a first step, we construct the grid:

# In[ ]:


from dune.grid import cartesianDomain
from dune.alugrid import aluConformGrid
vertices = numpy.array([(0,0), (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1)])
triangles = numpy.array([(2,0,1), (0,2,3), (4,0,3), (0,4,5), (6,0,5), (0,6,7)])
aluView = aluConformGrid({"vertices": vertices, "simplices": triangles})
aluView.hierarchicalGrid.globalRefine(7)


# In[ ]:


class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = grad
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        return self.grad

dim = aluView.dimension
p1ShapeFunctionSet = [LinearShapeFunction(1.0, [-1.0] * dim)]
for i in range(dim):
    p1ShapeFunctionSet.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))


# Let's get going. First we assemble the right hand side with
# $$
#     f(x) = 2\prod_{i} x_i\,(1-x_i)
# $$

# In[ ]:


from dune.geometry import quadratureRules
from dune.istl import blockVector

f = lambda v: sum(2.0 * x * (1 - x) for x in v)

dim, indexSet = aluView.dimension, aluView.indexSet
rhs = blockVector(indexSet.size(dim))

quadrature = quadratureRules(2)
for e in aluView.elements:
    geo = e.geometry
    for p in quadrature(e):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        for i, phi in enumerate(p1ShapeFunctionSet):
            index = indexSet.subIndex(e, i, dim)
            rhs[index] += w * phi.evaluate(x)[0] * f(geo.toGlobal(x))[0]


# Next, we assemble the stiffness matrix:

# In[ ]:


from dune.istl import bcrsMatrix, BCRSMatrix11

dim, indexSet = aluView.dimension, aluView.indexSet
matrix = bcrsMatrix((indexSet.size(dim), indexSet.size(dim)), 8, 0.1, BCRSMatrix11.implicit)

quadrature = quadratureRules(1)
for e in aluView.elements:
    geo = e.geometry
    for p in quadrature(e):
        x = p.position
        w = p.weight * geo.integrationElement(x)

        jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
        grads = [numpy.dot(jit, phi.gradient(x)) for phi in p1ShapeFunctionSet]

        for i, dphi in enumerate(grads):
            row = indexSet.subIndex(e, i, dim)
            for j, dpsi in enumerate(grads):
                matrix[row, indexSet.subIndex(e, j, dim)] += [[numpy.dot(dphi, dpsi) * w]]

_ = matrix.compress()


# And apply Dirichlet boundary conditions to the linear system:

# In[ ]:


from dune.geometry import referenceElement

for e in aluView.elements:
    for i in aluView.intersections(e):
        if not i.boundary:
            continue
        ref = referenceElement(e)
        j = i.indexInInside
        subs = [ref.subEntity(j, 1, k, dim) for k in range(ref.size(j, 1, dim))]
        rows = [indexSet.subIndex(e, s, dim) for s in subs]
        for r in rows:
            matrix[r] *= 0.0
            matrix[r, r] = [[1]]
            rhs[r] = 0.0


# In[ ]:


from dune.istl import CGSolver, SeqJacobi

solver = CGSolver(matrix.asLinearOperator(), SeqJacobi(matrix), 1e-10)

u = blockVector(aluView.indexSet.size(aluView.dimension))
_ = solver(u, rhs)


# To visualize the result, we need to construct a corresponding grid function:

# In[ ]:


@gridFunction(aluView)
def lgf( element, local ):
    return [ sum( phi.evaluate(local) * u[indexSet.subIndex(element, i, dim)]\
            for i, phi in enumerate(p1ShapeFunctionSet)) ]
_ = aluView.writeVTK("fem2d", pointdata={"u": lgf})


# In[ ]:


from dune.plotting import plotPointData
plotPointData(lgf, figsize=(9,9), gridLines=None)
