[Repository has been renamed please check our upgrade guide](UPGRADE.md)
========================================================================

Python Bindings for the DUNE Core
=================================

dune-python aims to provide Python bindings for the DUNE grid interface:

```python
import math
import dune.create as create
import dune.grid
# instanciate a yasp grid on a [0,1]^2 domain
domain = dune.grid.cartesianDomain([0, 0], [1, 1], [10, 10])
grid = create.grid("Yasp", domain)
# set up a simple grid function providing a global representation
gridfunction = grid.gridFunction(lambda x: [-(x[1] - 0.5)*math.sin(x[0]*12)])
# iterate over the elements of the grid and output the function t barycenters
lf = gridfunction.localFunction()
for element in grid.elements():
    lf.bind(element)
    x = [0.5,0.5]
    y = element.geometry.position(x)
    print("gridfunction( ", y, " ) = ", lf(x))
    lf.unbind()
# write a vtk file with a piece wise linear representtion of the function
grid.writeVTK("output", pointdata={'sin': gridfunction})
```

For a more complete example have a look at our [finite volume][fvcode] example.
In addition to bindings for some of the core Dune features dune-python also
provides tool to easily implement bindings for other Dune modules. This makes
it straightforward to, for example, provide high level bindings for Dune
discretization modules. With such bindings performance penalties are minimal
while all the flexibility of using Python for high level program flow control is
maintained.

See the file COPYING for full copying permissions.


Dependencies
------------

dune-python depends on the following DUNE modules:
- dune-common 2.6+
- dune-geometry 2.6+
- dune-grid 2.6+

In addition to the dependencies of these DUNE modules, the following software
packages are required:

- a C++14 compatible C++ compiler (e.g., GCC 5+, clang 3.8+)
- Python 2.7+ (regularly tested with 2.7, 3.4, and 3.5)
- The corresponding Python development package (i.e. the Python
  library and header file included for example in the `python-dev`
  package.

To get the most out of the examples we strongly recommend installing the following 
Python packages:

- numpy
- mpi4py

and also

- jupyter
- matplotlib


Quick Dive-In Using Docker
--------------------------

Users who simply want to use the functionality of `dune-python` as-is, e.g.,
for experimenting, can do so using Docker and a web browser.

Just type
```
docker run --rm -v dune:/dune -p 127.0.0.1:8888:8888 registry.dune-project.org/staging/dune-python
```
at the command prompt and connect to `localhost:8888` using your favorite web
browser.
Log into Jupyter using the password `dune` and have fun.

For your convenience, the demo notebooks are provided in the folder
`dune-python`.

With the above command, your notebooks will be kept persistent in a Docker
volume called `dune`.
To remove this volume, simply type
```
docker volume rm dune
```
at the command prompt.

To share python scripts and other files located in the current directory and also
share the output with the docker container use
```
docker run -it -v $PWD:/dune/work registry.dune-project.org/staging/dune-python bash
```
The content of the current directory is then available in the `work`
directory in the docker volume.

**Note for MAC users:** docker on MAC allocates 2GB to docker by default
which does not suffice for compiling the Dune shared libraries.
Please increase the value to at least 3GB.

For a quick test of the module
------------------------------

The following describes a simple procedure for testing `dune-python`
without performing a full installation - which is described further down in
this document.

__Note__: You can simply download a short [script][script]
which downloads and builds a very basic Dune setup.

Simply download the module, run `dunecontrol` and set the following
environment variables:

- set `PYTHONPATH` to point to the `python` subfolder in the
  `DUNE_PYTHON_BUILD_DIR`.
- set `DUNE_CONTROL_PATH` to point to the directory containing the dune core modules
  (at least `common`, `geometry`, `grid`).  If your core modules are globally installed,
  then your DUNE_CONTROL_PATH needs to include this global installation as well (this
  is different from the usual Dune dependency tracking, which finds globally installed
  modules even if they are not in DUNE_CONTROL_PATH).  On a Debian system, globally
  installed modules are in `/usr/lib/dunecontrol`.
- It is recommended that you also
  have `dune-alugrid` for some of the demos to work properly - if you want to use
  `dune-alugrid` add the build directory of `dune-alugrid` to the
  `PYTHONPATH`.

So use something like (for `bash`)

~~~
export DUNE_CONTROL_PATH=DUNELOCATION
export PYTHONPATH=DUNELOCATION/dune-python/build-cmake/python:DUNELOCATION/dune-alugrid/build-cmake/python
~~~

__There is one caveat__: since we are generating Python modules to be loaded
dynamically, we need to have all libraries compiled either as shared
libraries or from object files containing position independent code (PIC).
If your Dune modules have been installed globally by a Linux package manager,
they will automatically have these features.  If, on the other hand, you are
building Dune from the source, you need to add
`-DBUILD_SHARED_LIBS=TRUE` to your `CMAKE_FLAGS`.
Alternatively, you can add `-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE`.
All Dune modules need to be build in this manner.

There is a small Python script in the `demo` folder to test your
installation. Go to the `demo` folder and try

~~~
python test.py
~~~

If this does not give you an error (e.g. concerning a dune core module that was
not found), you have succeeded in setting up `dune-python`.
Note that this can take a moment to run since the `dune-py` is generated
in the `$HOME/.cache` folder. If there was a problem the file `test.log`
should contain additional information.

Examples
--------

Some examples showing how to use `dune-python` can be found in the `demo`
subdirectory of source tree of `dune-python`. More advanced example are
contained in `jupyter notebooks` in the `notebooks` folder. If you do not
have `jupyter` installed Python scripts are also available.

FULL INSTALLATION
=================

The approach given above gives you a very simple setup which will allow you to test 
the basic features of `dune-python`.
A more advanced installation using `pip` are explained in the following.

Using a Virtual Env
-------------------

If you want to install Dune Python bindings as a user, we recommend using a Python virtual env.
This will isolate your Dune packages from other Python modules.

To use this approach, just create a virtual env and activate it before
configuring your Dune modules for a system wide installation.
This way, the Python interpreter from your virtual env will automatically be used.
Note that Python packages are installed into a virtual env like system
packages are installed into the system. Therefore, you need to configure
your Dune modules as though you were doing a system wide installation,
but root privileges are not required.

You can also install all your DUNE modules into the virtual env by setting the
`CMAKE_INSTALL_PREFIX` to the virtual env.

The dune-py Module
------------------

The Dune code contains lots of templates and we cannot guess which ones the
Python user will instantiate at run time.
To resolve this problem, we build additional Python modules (C++ shared
libraries) on demand. A special Dune module called dune-py is used to build
these Python modules. It is special in the following sense:

- dune-py cannot be installed into the system, as some code will be generated at run time.
- Each user needs write access to a dune-py module and its build directory.
- Only one Python process may use a dune-py module at a time.

Therefore, we generate one dune-py module per user. The default position
is `${HOME}/.cache/dune-py`, but you can customize it through the environment
variable `DUNE_PY_DIR`. Notice that this directory should be exclusive to
each user to avoid race conditions. It is also advisable to locate this
directory on local storage for performance reasons.

In the following we describe two approaches for installing dune-python and
generating the dune-py module depending on your overall Dune setup.

[Working with a Preinstalled Dune](PACKAGEINSTALL.md)
-----------------------------------------------------

[Working with Dune Source Modules](SOURCEINSTALL.md)
----------------------------------------------------

Environment Variables
---------------------

When using dune-python from Python, a number of environment variables influence
its behaviour:

- `DUNE_LOG_LEVEL`: minimum level for logged messages.
  Choices are `debug`, `info`, `warning`, `error`.
  The default is `warning`.
- `DUNE_LOG_FORMAT`: format string for log messages.
  See documentation of Python's *logging* package.
- `DUNE_PY_DIR`: location of `dune-py` module.
  The default is `$HOME/.cache`.
- `DUNE_FORCE_BUILD`: if set to true, generated modules will be recompiled
  even if they already exist.
  The default is `false`.

Further Dune modules based on dune-python
-----------------------------------------

The following dune models can at moment be used together with dune-python:
- [dune-alugrid][alugridlink]: if you want to use dune-alugrid simply configure
  the module
  with the flags discussed above and run `make install_python` in the build
  directory (or use the `setup-dunepy` script in `dune-python`).
- [dune-spgrid][spgridlink]: also now provideds Python bindings.
- [dune-fempy][fempylink]: this is a new module providing high level Python
  bindings to the [dune-fem][femlink] module.
  You can use this to solve complex non linear partial
  differential equations from within Python. dune-fempy also makes use of the
  unified form language (UFL) to provide a descriptive language for entering
  the mathematical models.

Acknowledgements
---------------

For the C++ to Python bindings we use the great project [pybind11][pybind11] -
the sources are included in the dune-python module.
The initial code was implemented by Michaël Sghaïer during the
[Google Summer of Code][gsoc] program 2016.

[fvcode]: https://gitlab.dune-project.org/michael.sghaier/dune-python/blob/remove-database/demo/finitevolume.py
[alugridlink]: https://gitlab.dune-project.org/extensions/dune-alugrid
[spgridlink]: https://gitlab.dune-project.org/extensions/dune-spgrid
[fempylink]: https://gitlab.dune-project.org/dune-fem/dune-fempy
[femlink]: https://gitlab.dune-project.org/dune-fem/dune-fem
[pybind11]: https://github.com/pybind/pybind11
[gsoc]: https://summerofcode.withgoogle.com/
[script]: buildpy.sh
