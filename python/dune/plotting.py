import matplotlib
from matplotlib import pyplot
from numpy import amin, amax, linspace, linalg, random
from matplotlib.collections import PolyCollection
import os

try:
    from IPython.core.display import display
except:
    def display():
        pass
try:
    s = os.environ['DUNEPY_BLOCK_PLOTTING']
    block = s in ['TRUE','true', '1', 't', 'y', 'yes']
except KeyError:
    block = True

def _plotGrid(fig, grid, gridLines="black"):
    for p in grid.polygons():
        coll = PolyCollection(p, facecolor='none', edgecolor=gridLines, linewidth=0.5, zorder=2)
        pyplot.gca().add_collection(coll)


def plotGrid(grid, level=0, gridLines="black", figure=None,
        xlim=None, ylim=None, clim=None, figsize=None):
    if figure is None:
        figure = pyplot.figure(figsize=figsize)

    _plotGrid(figure, grid, gridLines=gridLines)

    figure.gca().set_aspect('equal')
    figure.gca().autoscale()
    if xlim:
        figure.gca().set_xlim(xlim)
    if ylim:
        figure.gca().set_ylim(ylim)

    pyplot.show(block=block)


def _plotPointData(fig, grid, solution, level=0, gridLines="black",
        component=None, vectors=None, nofVectors=None,
        xlim=None, ylim=None, clim=None, cmap=None, colorbar=True):

    if (gridLines is not None) and (gridLines != ""):
        _plotGrid(fig, grid, gridLines=gridLines)

    if solution is not None:
        triangulation = grid.triangulation(level)
        data = solution.pointData(level)
        try:
            x1 = vectors[0]
            x2 = vectors[1]
            if x1 >= solution.dimRange or x2 >= solution.dimRange:
                vectors = None
        except:
            vectors = None

        if not vectors == None:
            if nofVectors==0:
                nofVector = len(triangulation.x)
            idx = random.randint(len(triangulation.x),size=nofVectors)
            pyplot.quiver(triangulation.x[idx], triangulation.y[idx],
                      data[idx][:,x1], data[idx][:,x2],
                      units='xy', scale=10., zorder=3, color='blue',
                      width=0.007, headwidth=3., headlength=4.)
        else:
            if component is None:
              if solution.dimRange > 1:
                 data = linalg.norm(data,axis=1)
              else:
                  data = data[:,0]
            else:
              data = data[:,component]
            minData = amin(data)
            maxData = amax(data)
            if clim == None:
                clim = [minData, maxData]
            levels = linspace(clim[0], clim[1], 256, endpoint=True)
            pyplot.tricontourf(triangulation, data, cmap=cmap, levels=levels, extend="both")
            if colorbar:
                # having extend not 'both' does not seem to work (needs fixing)...
                if clim[0] > minData and clim[1] < maxData:
                    extend = 'both'
                elif clim[0] > minData:
                    extend = 'min'
                elif clim[1] < maxData:
                    extend = 'max'
                else:
                    extend = 'neither'
                v = linspace(clim[0], clim[1], 10, endpoint=True)
                norm = matplotlib.colors.Normalize(vmin=clim[0], vmax=clim[1])
                cbar = pyplot.colorbar(orientation="vertical",shrink=1.0,
                          extend=extend, norm=norm, ticks=v)
                cbar.ax.tick_params(labelsize=18)

    fig.gca().set_aspect('equal')
    fig.gca().autoscale()
    if xlim:
        fig.gca().set_xlim(xlim)
    if ylim:
        fig.gca().set_ylim(ylim)


def plotPointData(solution, level=0, gridLines="black",
        vectors=None, nofVectors=None, figure=None,
        xlim=None, ylim=None, clim=None, figsize=None, cmap=None):
    try:
        grid = solution.grid
    except:
        grid = solution
        solution = None
    if not grid.dimension == 2:
        print("inline plotting so far only available for 2d grids")
        return

    if figure is None:
        figure = pyplot.figure(figsize=figsize)
    _plotPointData(figure,grid,solution,level,gridLines,None,vectors,nofVectors,xlim,ylim,clim,cmap,True)

    pyplot.show(block=block)

def plotComponents(solution, level=0, show=None, gridLines="black", figure=None,
        xlim=None, ylim=None, clim=None, figsize=None, cmap=None):
    try:
        grid = solution.grid
    except:
        grid = solution
        solution = None
    if not grid.dimension == 2:
        print("inline plotting so far only available for 2d grids")
        return

    if not show:
        show = range(solution.dimRange)

    if figure is None:
        figure = pyplot.figure(figsize=figsize)
    offset = 1 if (gridLines is not None) and (gridLines != "") else 0
    subfig = 101+(len(show)+offset)*10

    # first the grid if required
    if (gridLines is not None) and (gridLines != ""):
        pyplot.subplot(subfig)
        _plotPointData(figure,grid,None,level,gridLines,None,False,None,xlim,ylim,clim,cmap)

    # add the data
    for p in show:
        pyplot.subplot(subfig+offset+p)
        _plotPointData(figure,grid,solution,level,"",p,False,None,xlim,ylim,clim,cmap,False)

    pyplot.show(block=block)

def mayaviPointData(solution, level=0, component=0):
    grid = solution.grid
    from mayavi import mlab
    triangulation = grid.triangulation(level)
    z = solution.pointData(level)[:,component]
    s = mlab.triangular_mesh(triangulation.x, triangulation.y, z,
                                triangulation.triangles)
    mlab.show(block=block)
