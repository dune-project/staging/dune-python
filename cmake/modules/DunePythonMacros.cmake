dune_python_require_version("2.7")

if(NOT PYTHONINTERP_FOUND)
  message(FATAL_ERROR "Module dune-python requires a Python interpreter")
endif()
if(NOT PYTHONLIBS_FOUND)
  message(FATAL_ERROR "Found a Python interpreter but module dune-python also requires the Python libraries (e.g. a python-dev package)")
endif()

include_directories("${PYTHON_INCLUDE_DIRS}")

function(add_python_targets base)
  include(DuneSymlinkOrCopy)
  foreach(file ${ARGN})
    dune_symlink_to_source_files(FILES ${file}.py)
  endforeach()
endfunction()

include(DuneAddPybind11Module)
