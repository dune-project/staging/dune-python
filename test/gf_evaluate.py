import numpy
from dune.grid import gridFunction


from dune.alugrid import aluConformGrid
vertices = numpy.array([(0,0), (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1)])
triangles = numpy.array([(2,0,1), (0,2,3), (4,0,3), (0,4,5), (6,0,5), (0,6,7)])
aluView = aluConformGrid({"vertices": vertices, "simplices": triangles})
aluView.hierarchicalGrid.globalRefine(14)

fcalls = 0
@gridFunction(aluView)
def f(x):
    global fcalls
    fcalls += 1
    return x[0]*x[1]
gcalls = 0
@gridFunction(aluView)
def g(e,x):
    global gcalls
    gcalls += 1
    return e.geometry.position(x)

e = aluView.elements.__next__()
xLoc = numpy.array([[0,0.1,0.2,0.3],[0,0.4,0.6,0.8]])
xGlb = e.geometry.position(xLoc)

fcalls = 0
gcalls = 0
y=f(xGlb)
# print( y, fcalls)
assert fcalls == 1
y = g(e, xLoc)
# print( y, gcalls)
assert gcalls == 1

fcalls = 0
y = f(e,xLoc)
# print( y, fcalls)
assert fcalls == 1

fcalls = 0
gcalls = 0
lf = f.localFunction()
lg = g.localFunction()
lf.bind(e)
lg.bind(e)
y = lf(xLoc)
# print( y, fcalls)
assert fcalls == 1
y = lg(xLoc)
# print( y, gcalls)
assert gcalls == 1
lg.unbind()
lf.unbind()
