from __future__ import print_function

import math

# dune modules
import dune.grid as grid
import dune.create as create

from dune.grid import gridFunction, GridFunction

###################################################

# just get the grid (only for testing - not used)
# onedgrid = grid.oneDGrid("../data/unitcube-1d.dgf")
#onedgrid = grid.create("OneD", "../data/unitcube-1d.dgf")
onedgrid = create.grid("OneD", "../data/unitcube-1d.dgf")

for element in onedgrid.elements:
    geo = element.geometry
    print("Center ", geo.center)
    for corner in geo.corners:
        print("Corner ", corner)
    print(geo.corner())

# get the full grid module and then the grid (module needed for grid # functions and output object)
yaspgrid = create.grid("Yasp", "../data/unitcube-2d.dgf", dimgrid=2)
yaspgrid.hierarchicalGrid.globalRefine(2)

vtk_yaspgrid = yaspgrid.vtkWriter()

@gridFunction(yaspgrid)
def expr_global(x):
    return [-(x[1] - 0.5)*math.sin(x[0]*12)]

expr_global.addToVTKWriter("expr_global", vtk_yaspgrid, grid.DataType.PointData)
lf = expr_global.localFunction()
for element in yaspgrid.elements:
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.toGlobal(x)
    print("ggf( ", y, " ) = ", lf(x), " | ", expr_global(y))
    lf.unbind()

@gridFunction(yaspgrid)
def expr_local(e, x):
    return [abs(expr_global(x)[0] - expr_global(e.geometry.center)[0]), expr_global(x)[0]]

expr_local.addToVTKWriter("expr_local", vtk_yaspgrid, grid.DataType.PointData)
lf = expr_local.localFunction()
for element in yaspgrid.elements:
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.toGlobal(x)
    print("expr_local( ", y, " ) = ", lf(x), " | ", expr_local(element, x))
    lf.unbind()

@GridFunction(yaspgrid)
class ExprLocal:
  def __init__(self,gf):
    self.gf_ = gf
  def __call__(self,entity,point):
    geo = entity.geometry
    return [abs(self.gf_(entity,point)[0] - self.gf_(geo.center)[0]), self.gf_(geo.toGlobal(point))[0]]

lgf = ExprLocal(expr_global)
lgf.addToVTKWriter("ExprLocal", vtk_yaspgrid, grid.DataType.PointData)
lf = lgf.localFunction()
for element in yaspgrid.elements:
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.toGlobal(x)
    print("lgf( ", y, " ) = ", lf(x), " | ", expr_local(element,x))
    lf.unbind()

@GridFunction(yaspgrid)
class ExprGlobal:
  def __init__(self,gf):
    self.gf_ = gf
  def __call__(self,point):
    return [abs(self.gf_(point)[0] - self.gf_([0,0])[0]), self.gf_(point)[0]]

lgf = ExprGlobal(expr_global)
lgf.addToVTKWriter("ExprLocal", vtk_yaspgrid, grid.DataType.PointData)
lf = lgf.localFunction()
for element in yaspgrid.elements:
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.toGlobal(x)
    print("lgf( ", y, " ) = ", lf(x), " | ", expr_local(element,x))
    lf.unbind()
# remove all variables in python to make sure they stay alive long enough for the vtk # writer
expr_local = None
# expr_global = None

vtk_yaspgrid.write("yaspgrid_demo");

######################################################

try:
    import numpy
    from scipy.spatial import Delaunay
    import matplotlib.pyplot as plt
    n_radii, n_angles = 5, 10
    radii = numpy.linspace(1.0 / n_radii, 1.0, n_radii)
    angles = numpy.linspace(0, 2*numpy.pi, n_angles, endpoint=False)
    angles = numpy.repeat(angles[..., numpy.newaxis], n_radii, axis=1)
    x = numpy.append(0, (radii*numpy.cos(angles)).flatten())
    y = numpy.append(0, (radii*numpy.sin(angles)).flatten())
    points = numpy.stack((x,y), axis=-1)
    triangles = Delaunay(points).simplices
    plt.triplot(points[:,0], points[:,1], triangles.copy())
    plt.plot(points[:,0], points[:,1], 'o')
    plt.show()
    alugrid = create.grid("ALUConform", {'vertices':points, 'simplices':triangles})
except:
    alugrid = create.grid("ALUConform", "../data/unitcube-2d.dgf", dimgrid=2)

output = alugrid.vtkWriter()
output.write("alugrid_demo")

from dune.alugrid import aluConformGrid
surface = aluConformGrid("../data/sphere.dgf", dimgrid=2, dimworld=3)
surface.writeVTK("surface_demo", celldata={"angle" : lambda x: math.atan2(x[1],x[0]) })

t = 0
vtk = surface.sequencedVTK("surface_seq_demo", celldata={"angle" : lambda x: math.sin(t*math.pi)*math.atan2(x[1],x[0]) })
vtk()
while t<2:
    t += 0.1
    vtk()
