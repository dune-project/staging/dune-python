from __future__ import print_function

import sys
import math

import dune.grid
try:
    from dune.alugrid import aluCubeGrid
except ImportError:
    print("""
    This demo requires a grid with local adaptivity.
    By default dune-alugrid is used but that module could not be found.
    Either install dune-alugrid or switch to a grid which supports local adaptivity.
    """)
    sys.exit(0)

domain = dune.grid.cartesianDomain([0, 0], [1, 1], [10, 10])
grid = aluCubeGrid(domain)

t = 0
maxLevel = 4
hgrid = grid.hierarchicalGrid
marker = dune.grid.Marker
vtk = grid.vtkWriter()

def mark(element):
    y = element.geometry.center - [0.5+0.2*math.cos(t), 0.5+0.2*math.sin(t)]
    if y.two_norm2 < 0.2*0.2 and y.two_norm2 > 0.1*0.1:
      return marker.refine if element.level < maxLevel else marker.keep
    else:
      return marker.coarsen

for i in range(0,maxLevel):
    hgrid.mark(mark)
    hgrid.adapt()
    hgrid.loadBalance()

nr = 0
while t < 2*math.pi:
    print('time:',t)
    hgrid.mark(mark)
    hgrid.adapt()
    hgrid.loadBalance()
    vtk.write("adapt-demo", nr)
    print(grid.size(0))
    t = t+0.1
    nr = nr+1
