import time
import numpy

from dune.grid import cartesianDomain
from dune.alugrid import aluConformGrid as grid
from dune.geometry import quadratureRule, referenceElement

from dune.istl import bcrsMatrix, BlockVector, BuildMode, CGSolver, SeqJacobi

class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = grad
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        return self.grad


def P1ShapeFunctionSet(dim):
    sfs = [LinearShapeFunction(1.0, [-1.0] * dim)]
    for i in range(dim):
        sfs.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))
    return sfs


def assembleIstl(gridView, mapper, basis):
    numDofs = len(mapper)
    matrix = bcrsMatrix((numDofs, numDofs), 10, 0.1, BuildMode.implicit)

    for e in gridView.elements:
        geo = e.geometry

        gt  = e.type
        if not gt.isSimplex:
            raise NotImplementedError("finite element shape functions only implemented for simplex elements")

        indices = mapper(e)
        for p in quadratureRule(gt, 1):
            x = p.position
            w = p.weight * geo.integrationElement(x)

            jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, phi.gradient(x)) for phi in basis]

            for i, dphi in enumerate(grads):
                for j, dpsi in enumerate(grads):
                    matrix[indices[i], indices[j]] += [[numpy.dot(dphi, dpsi) * w]]

    matrix.compress()

    return matrix

def assembleScipy(gridView, mapper, basis):
    localEntries = len(basis)
    maxEntries = localEntries**2*gridView.size(0)
    value = numpy.zeros(maxEntries)
    rowIndex, colIndex = numpy.zeros(maxEntries,int), numpy.zeros(maxEntries,int)

    count = 0
    for e in gridView.elements:
        geo = e.geometry

        gt  = e.type
        if not gt.isSimplex:
            raise NotImplementedError("finite element shape functions only implemented for simplex elements")

        indices = mapper(e)[:]
        localMatrix = numpy.zeros([localEntries,localEntries])

        for p in quadratureRule(gt, 1):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, phi.gradient(x)) for phi in basis]
            for i, dphi in enumerate(grads):
                for j, dpsi in enumerate(grads):
                    localMatrix[i,j] += numpy.dot(dphi, dpsi) * w
        for i in range(localEntries):
            for j in range(localEntries):
                value[count] = localMatrix[i,j]
                rowIndex[count] = indices[i]
                colIndex[count] = indices[j]
                count += 1

    from scipy.sparse import coo_matrix
    return coo_matrix((value, (rowIndex, colIndex))).tocsr()


domain = {'vertices': numpy.array([(0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0)]), 'simplices': numpy.array([(0, 1, 2), (0, 2, 3)])}
gridView = grid(domain)
gridView.hierarchicalGrid.globalRefine(14)

mapper = gridView.mapper(lambda gt: gt.dim == 0)
basis = P1ShapeFunctionSet(gridView.dimension)

print("Assembling...")
start = time.time()
istlA = assembleIstl(gridView, mapper, basis)
print("time used:", round(time.time()-start,2))

print("Assembling...")
start = time.time()
scipyA = assembleScipy(gridView, mapper, basis)
print("time used:", round(time.time()-start,2))
