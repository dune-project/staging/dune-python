import time
import numpy
import math
import dune.plotting

import dune.geometry
from dune.grid import gridFunction

dune.plotting.block = False

from dune.grid import cartesianDomain, yaspGrid
from dune.alugrid import aluConformGrid
domain = cartesianDomain([0, 0], [1, 0.25], [400, 100])
aluView = aluConformGrid(domain)

class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = numpy.array(grad)
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        try:
            return numpy.array([self.grad,]*local.shape[1]).transpose()
        except (AttributeError, IndexError):
            return self.grad
dim = aluView.dimension
p1ShapeFunctionSet = [LinearShapeFunction(1.0, [-1.0] * dim)]
for i in range(dim):
    p1ShapeFunctionSet.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))

@gridFunction(aluView)
def f(x): return 2.0*x[0]*(1.-x[1])

dim = aluView.dimension
mapper = aluView.mapper(lambda gt: gt.dim == 0)

# first do a rhs assmbly by hand
start = time.time()
rhs = numpy.zeros(len(mapper))
for e in aluView.elements:
    geo = e.geometry
    for p in dune.geometry.quadratureRule(e.type, 3):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        y = geo.toGlobal(x)
        for i, phi in enumerate(p1ShapeFunctionSet):
            index = mapper.subIndex(e, i, dim)
            rhs[index] += w * phi.evaluate(x) * f(y)
print("time used:", round(time.time()-start,2))

# now use vectorization (using the negative rhs)
start = time.time()
@gridFunction(aluView)
def fphi(e,x):
    return numpy.array([ f(e.geometry.toGlobal(x)) * phi.evaluate(x) for phi in p1ShapeFunctionSet ])
rules = dune.geometry.quadratureRules(3)
for e in aluView.elements:
    rhs[mapper(e)[:]] -= dune.geometry.integrate(rules,e,fphi)[:]
print("time used:", round(time.time()-start,2))

assert not numpy.any(numpy.abs(rhs)>1e-15), "assembly of rhs failed"

##############################################################

# Matrix assmbly
localEntries = len(p1ShapeFunctionSet)
maxEntries = localEntries**2*aluView.size(0)
value = numpy.zeros(maxEntries)
rowIndex, colIndex = numpy.zeros(maxEntries,int), numpy.zeros(maxEntries,int)

# first agian by hand
start = time.time()
count = 0
for e in aluView.elements:
    geo = e.geometry
    indices = numpy.zeros(localEntries)
    localMatrix = numpy.zeros([localEntries,localEntries])
    for p in dune.geometry.quadratureRule(e.type, 3):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
        grads = [numpy.dot(jit, phi.gradient(x)) for phi in p1ShapeFunctionSet]
        for i, dphi in enumerate(grads):
            for j, dpsi in enumerate(grads):
                localMatrix[i,j] += numpy.dot(dphi, dpsi) * w
    # for i in range(localEntries):
    #     indices[i] = mapper.subIndex(e, i, dim)
    indices = mapper(e)[:]
    for i in range(localEntries):
        for j in range(localEntries):
            value[count] = localMatrix[i,j]
            rowIndex[count] = indices[i]
            colIndex[count] = indices[j]
            count += 1
print("time used:", round(time.time()-start,2))

# no gridfunction yet available for matrix valued functions
def gphigphi(e,x):
    hatgrads = numpy.array([ phi.gradient(x) for phi in p1ShapeFunctionSet])
    ##  nofFct x dimGrid x nofPnt -> nofFct x dimWorld x nofPnt
    grads = e.geometry.pushForwardGradients(x,hatgrads)[:]
    ret = numpy.array([
        numpy.dot( grads[:,:,p],grads[:,:,p].transpose() )
         for p in range(x.shape[1])
       ]).transpose()
    return ret[:]
# now use vectorization (subtracting again)
start = time.time()

localMatrix = numpy.zeros([localEntries,localEntries])
rules = dune.geometry.quadratureRules(3)
count = 0
for e in aluView.elements:
    indices = mapper(e)[:]
    localMatrix[:] = dune.geometry.integrate(rules,e,gphigphi)[:]
    value[count:count+localEntries**2] -= localMatrix.reshape([localEntries**2,])
    rowIndex[count:count+localEntries**2] -= \
              list( (int(i) for i in indices for _ in range(localEntries) ) )
    colIndex[count:count+localEntries**2] -= \
              list( (int(i) for _ in range(localEntries) for i in indices ) )
    count += localEntries**2
    # dense: A[numpy.ix_(indices, indices)] -= dune.geometry.integrate(rules,e,gphigphi)
print("time used:", round(time.time()-start,2))

assert not numpy.any(numpy.abs(value)>1e-15), "assembly of matrix failed"
assert not numpy.any(numpy.abs(rowIndex)>1e-15), "assembly of matrix failed"
assert not numpy.any(numpy.abs(colIndex)>1e-15), "assembly of matrix failed"
